# Copyright (C) 2010 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This file is the build configuration for a full Android
# build for grouper hardware. This cleanly combines a set of
# device-specific aspects (drivers) with a device-agnostic
# product configuration (apps).
#

#JCROM
$(call inherit-product-if-exists, jcrom/asus/tilapia/device-common.mk)

# Inherit from those products. Most specific first.
$(call inherit-product, device/asus/tilapia/device.mk)
# This is where we'd set a backup provider if we had one
#$(call inherit-product, device/sample/products/backup_overlay.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base.mk)

#JCROM
$(call inherit-product-if-exists, jcrom/asus/tilapia/device-tilapia.mk)

PRODUCT_BUILD_PROP_OVERRIDES += PRODUCT_NAME=nakasig BUILD_FINGERPRINT=google/nakasig/tilapia:4.2.2/JDQ39/573038:user/release-keys PRIVATE_BUILD_DESC="nakasig-user 4.2.2 JDQ39 573038 release-keys"

# Discard inherited values and use our own instead.
PRODUCT_NAME := full_tilapia
PRODUCT_DEVICE := tilapia
PRODUCT_BRAND := Google
PRODUCT_MODEL := Nexus 7
PRODUCT_MANUFACTURER := Asus

